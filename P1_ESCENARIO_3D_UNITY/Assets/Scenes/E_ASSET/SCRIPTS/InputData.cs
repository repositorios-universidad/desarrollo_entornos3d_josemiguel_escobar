using UnityEngine;
using System;

[Serializable]

public struct InputData
{
    //basic movement
    public float hMovement;
    public float vMovement;

    //mouse rotation
    public float verticalMouse;
    public float horizontalMouse;

    //extra movement
    public bool dash;
    public bool jump;

    public void getInput()
    {
        //basic movement
        hMovement = Input.GetAxis("Horizontal");
        vMovement = Input.GetAxis("Vertical");

        //Mouse/joystick rotation 
        verticalMouse = Input.GetAxis("Mouse Y");
        horizontalMouse = Input.GetAxis("Mouse X");

        //Extra movement

        dash = Input.GetButton("Dash");
        jump = Input.GetButton("Jump");

    }

    public void resetInput()
    {
        //basic movement
        hMovement = vMovement = verticalMouse = horizontalMouse = 0;

        dash = jump = false;

    }



}
