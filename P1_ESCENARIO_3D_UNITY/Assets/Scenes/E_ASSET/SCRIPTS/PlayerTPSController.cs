using UnityEngine;
using System;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    //public UnityEvents onInteractionInput;
    private InputData input;
    private CharacterAninBasedMovement characterMovement;

    public bool blockInput { get; set;}

    public bool onInteractionZone { get; set; }

    public static event Action OnInteractionInput;


    void Start()
    {
        characterMovement = GetComponent<CharacterAninBasedMovement>();
    }

    void Update()
    {
        if (blockInput)
        {
            input.resetInput();
        }
        else
        {
            //Get input from player
            input.getInput();
        }


        if (onInteractionZone && input.jump)
        {
                OnInteractionInput?.Invoke();
        }
        else
        {
            //Move the character
            characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
        }
       
    }

}