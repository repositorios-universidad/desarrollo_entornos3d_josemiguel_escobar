using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]

public class CharacterAninBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationThreshhold = 0.3f;
    public int degreesToTurn = 160;
    public float jumpForce = 8f; // Fuerza de salto

    [Header("Animation Parameters")]
    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180Param = "turn180";
    public string idleParam = "idle";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;

    private float idleTimer = 0f;
    private const float idleChangeTime1 = 20f; // Tiempo para cambiar a idle 1
    private const float idleChangeTime2 = 40f; // Tiempo para cambiar a idle 2
    private const float resetTime = 1f; // Tiempo para reiniciar el temporizador y el par�metro idle cuando la velocidad supere 0.5f

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {
        // Calcular la magnitud de la entrada
        Speed = new Vector2(hInput, vInput).magnitude;

        // Dash solo si el personaje ha alcanzado la velocidad m�xima (valor del par�metro del animador)
        if (Speed >= Speed - rotationThreshhold && dash)
        {
            Speed = 1.5f;
        }

        // Salto
        if (jump && characterController.isGrounded) // Si se presiona el bot�n de salto y est� en el suelo
        {
            animator.SetBool("jump", true); // Activar el par�metro de salto en el Animator
            Jump();
        }
        else
        {
            animator.SetBool("jump", false); // Desactivar el par�metro de salto en el Animator
        }

        // Mover f�sicamente al jugador
        if (Speed > rotationThreshhold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDirection = forward * vInput + right * hInput;

            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
            {
                turn180 = true;
            }
            else
            {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);
            }

            animator.SetBool(turn180Param, turn180);

            // Reiniciar el temporizador y el par�metro idle si la velocidad supera 0.5f
            if (Speed > 0.5f)
            {
                idleTimer = 0f;
                animator.SetFloat(idleParam, 0f);
            }
        }
        else if (Speed < rotationThreshhold)
        {
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            // Detener al personaje
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);

            // Incrementar el temporizador de idle
            idleTimer += Time.deltaTime;

            // Cambiar a idle 1 despu�s de 20 segundos y a idle 2 despu�s de 40 segundos si la velocidad es menor a 1f
            if (idleTimer >= idleChangeTime1 && idleTimer < idleChangeTime2 && Speed < 0.5f)
            {
                animator.SetFloat(idleParam, 1f);
            }
            else if (idleTimer >= idleChangeTime2 && Speed < 0.5f)
            {
                animator.SetFloat(idleParam, 2f);
            }
            else if (idleTimer >= idleChangeTime2)
            {
                animator.SetFloat(idleParam, 0f);
                idleTimer = 0f; // Reiniciar el temporizador cuando se mueva
            }
        }
    }

    void Jump()
    {
        // Aplicar la fuerza de salto hacia arriba
        characterController.Move(Vector3.up * jumpForce * Time.deltaTime);
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (Speed < rotationThreshhold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if (distanceToRightFoot > distanceToLeftFoot)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }
    }
}